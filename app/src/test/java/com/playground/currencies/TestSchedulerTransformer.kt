package com.playground.currencies

import com.playground.currencies.domain.transformer.SchedulerTransformer
import io.reactivex.*


class TestSchedulerTransformer(
    private val subscribeScheduler: Scheduler,
    private val observeScheduler: Scheduler
) : SchedulerTransformer {

    override fun <T> applyObservableSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { it
            .subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
        }
    }

    override fun applyCompletableSchedulers(): CompletableTransformer {
        return CompletableTransformer { it
            .subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
        }
    }

    override fun <T> applySingleSchedulers(): SingleTransformer<T, T> {
        return SingleTransformer { it
            .subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
        }
    }

    override fun applySubscribeScheduler(completable: Completable): Completable {
        return completable.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(single: Single<T>): Single<T> {
        return single.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(observable: Observable<T>): Observable<T> {
        return observable.subscribeOn(subscribeScheduler)
    }

    override fun getSubscribeScheduler(): Scheduler {
        return subscribeScheduler
    }
}
