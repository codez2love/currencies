package com.playground.currencies.data.store

import com.playground.currencies.GenericTestObjects
import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.CurrencyConverter
import com.playground.currencies.domain.CurrencyExchangeRate
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.math.BigDecimal

@RunWith(JUnit4::class)
class CurrencyConverterImplTest {

    @Test
    fun `check sort is applied to exchange rate list`() {
        val testCurrency = GenericTestObjects.generateDummyCurrency(id = "RON", rate = "1")
        val currencyConverter: CurrencyConverter = CurrencyConverterImpl()
        val testInput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("GDP", BigDecimal("1")),
                Currency("DKK", BigDecimal("1")),
                Currency("AUD", BigDecimal("1")),
                Currency("BGN", BigDecimal("1")),
                Currency("EUR", BigDecimal("1"))
            )
        )
        val expectedTestOuput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("AUD", BigDecimal("1")),
                Currency("BGN", BigDecimal("1")),
                Currency("DKK", BigDecimal("1")),
                Currency("EUR", BigDecimal("1")),
                Currency("GDP", BigDecimal("1"))
            )
        )

        val actualOutput = currencyConverter.applyRates(testCurrency, testInput)
        assertEquals(expectedTestOuput, actualOutput)
    }

    @Test
    fun `check selected currency is removed from the exchange rate list`() {
        val testCurrency = GenericTestObjects.generateDummyCurrency(id = "EUR", rate = "1")
        val currencyConverter: CurrencyConverter = CurrencyConverterImpl()
        val testInput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("AUD", BigDecimal("1")),
                Currency("BGN", BigDecimal("1")),
                Currency("DKK", BigDecimal("1")),
                Currency("GDP", BigDecimal("1")),
                Currency("EUR", BigDecimal("1"))
            )
        )
        val expectedTestOuput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("AUD", BigDecimal("1")),
                Currency("BGN", BigDecimal("1")),
                Currency("DKK", BigDecimal("1")),
                Currency("GDP", BigDecimal("1"))
            )
        )

        val actualOutput = currencyConverter.applyRates(testCurrency, testInput)
        assertEquals(expectedTestOuput, actualOutput)
    }

    @Test
    fun `check selected currency value is multiplied to each item in exchange rate list`() {
        val testValue = BigDecimal("2")
        val testCurrency = Currency("EUR", testValue)
        val currencyConverter: CurrencyConverter = CurrencyConverterImpl()
        val testInput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("AUD", BigDecimal("1")),
                Currency("BGN", BigDecimal("2")),
                Currency("DKK", BigDecimal("3")),
                Currency("GDP", BigDecimal("4"))
            )
        )
        val expectedTestOuput = CurrencyExchangeRate(
            selectedCurrency = testCurrency,
            exchangeRate = listOf(
                Currency("AUD", BigDecimal("1").multiply(testValue), BigDecimal("1")),
                Currency("BGN", BigDecimal("2").multiply(testValue), BigDecimal("2")),
                Currency("DKK", BigDecimal("3").multiply(testValue), BigDecimal("3")),
                Currency("GDP", BigDecimal("4").multiply(testValue), BigDecimal("4"))
            )
        )

        val actualOutput = currencyConverter.applyRates(testCurrency, testInput)
        assertEquals(expectedTestOuput, actualOutput)
    }


}