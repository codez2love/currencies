package com.playground.currencies

import com.playground.currencies.domain.transformer.DebugTransformer
import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer


class TestDebugTransformer : DebugTransformer {

    override fun <T> applyObservableDebugger(tag: String): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it
                .doOnNext { data ->
                    println(
                        String.format(
                            "[%s] [onNext] [Thread: %s] [observable] [response: %s]",
                            tag,
                            Thread.currentThread().name,
                            data.toString()
                        )
                    )
                }
                .doOnSubscribe {
                    println(
                        String.format(
                            "[%s] [subscribe] [Thread:%s] [observable]",
                            tag,
                            Thread.currentThread().name
                        )
                    )
                }
                .doOnDispose {
                    println(String.format("[%s] [dispose] [observable]", tag))
                }
        }
    }

    override fun applyCompletableDebugger(tag: String): CompletableTransformer {
        return CompletableTransformer {
            it
                .doOnComplete {
                    println(
                        String.format(
                            "[%s] [onComplete] [Thread:%s] [completable]",
                            tag,
                            Thread.currentThread().name
                        )
                    )
                }
                .doOnSubscribe {
                    println(
                        String.format(
                            "[%s] [subscribe] [Thread:%s] [completable]",
                            tag,
                            Thread.currentThread().name
                        )
                    )
                }
                .doOnDispose {
                    println(String.format("[%s] [dispose] [completable]", tag))
                }
        }
    }

    override fun <T> applySingleDebugger(tag: String): SingleTransformer<T, T> {
        return SingleTransformer {
            it
                .doOnSuccess { data ->
                    println(
                        String.format(
                            "[%s] [onSuccess] [Thread:%s] [single] [response: %s] ",
                            tag,
                            Thread.currentThread().name,
                            data.toString()
                        )
                    )
                }
                .doOnSubscribe {
                    println(
                        String.format(
                            "[%s] [subscribe] [Thread:%s] [single]",
                            tag,
                            Thread.currentThread().name
                        )
                    )
                }
                .doOnDispose {
                    println(String.format("[%s] [dispose] [single]", tag))
                }
        }
    }
}
