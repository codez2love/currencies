package com.playground.currencies.domain.interactors

import com.playground.currencies.GenericTestObjects
import com.playground.currencies.TestDebugTransformer
import com.playground.currencies.TestSchedulerTransformer
import com.playground.currencies.domain.ConfigProvider
import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.transformer.DebugTransformer
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import org.mockito.Mockito.`when` as on

@RunWith(MockitoJUnitRunner.Silent::class)
class FetchOnIntervalUseCaseTest {

    @Mock
    lateinit var fetchCurrencyUseCase: FetchCurrencyUseCase
    @Mock
    lateinit var getIntervalTriggerUseCase: GetIntervalTriggerUseCase
    @Mock
    lateinit var configProvider: ConfigProvider

    @InjectMocks
    lateinit var fetchOnIntervalUseCase: FetchOnIntervalUseCase

    private val debugTransformer: DebugTransformer = TestDebugTransformer()
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setUp() {
        testScheduler = TestScheduler()
        fetchOnIntervalUseCase.setDebugTransformer(debugTransformer)
        fetchOnIntervalUseCase.setSchedulerTransformer(
            TestSchedulerTransformer(testScheduler, testScheduler)
        )
    }

    @Test
    fun `when fetchCurrency returns same result multiple times pass only 1 result through`() {
        val testCurrency = Currency("EUR", BigDecimal("1"))
        val pullRate = 5

        on(configProvider.getPullRate()).thenReturn(pullRate)
        on(fetchCurrencyUseCase.chain(testCurrency))
            .thenReturn(Single.just(GenericTestObjects.buildCurrencyExchangeRate()))
        on(getIntervalTriggerUseCase.chain(pullRate))
            .thenReturn(Observable.just(1, 2, 3))


        val testObserver = fetchOnIntervalUseCase[testCurrency].test()
        testScheduler.triggerActions()


        testObserver.assertValueCount(1)
        verify(fetchCurrencyUseCase, times(3)).chain(testCurrency)

        testObserver.dispose()
    }

    @Test
    fun `when fetchCurrency returns different result pass only different results through`() {
        val testCurrency = Currency("EUR", BigDecimal("1"))
        val pullRate = 5

        on(configProvider.getPullRate()).thenReturn(pullRate)
        on(fetchCurrencyUseCase.chain(testCurrency))
            .thenReturn(
                Single.just(GenericTestObjects.buildCurrencyExchangeRate()),
                Single.just(GenericTestObjects.buildCurrencyExchangeRate("2.0"))
            )
        on(getIntervalTriggerUseCase.chain(pullRate))
            .thenReturn(Observable.just(1, 2, 3, 4, 5))


        val testObserver = fetchOnIntervalUseCase[testCurrency].test()
        testScheduler.triggerActions()


        testObserver.assertValueCount(2)
        verify(fetchCurrencyUseCase, times(5)).chain(testCurrency)

        testObserver.dispose()
    }


}