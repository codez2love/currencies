package com.playground.currencies.domain.interactors

import com.playground.currencies.GenericTestObjects
import com.playground.currencies.TestDebugTransformer
import com.playground.currencies.TestSchedulerTransformer
import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.CurrencyRepo
import com.playground.currencies.domain.transformer.DebugTransformer
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import org.mockito.Mockito.`when` as on

@RunWith(MockitoJUnitRunner.Silent::class)
class FetchCurrencyUseCaseTest {

    @Mock
    lateinit var currencyRepo: CurrencyRepo

    @InjectMocks
    lateinit var fetchCurrencyUseCase: FetchCurrencyUseCase

    private val debugTransformer: DebugTransformer = TestDebugTransformer()
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setUp() {
        testScheduler = TestScheduler()
        fetchCurrencyUseCase.setDebugTransformer(debugTransformer)
        fetchCurrencyUseCase.setSchedulerTransformer(
            TestSchedulerTransformer(testScheduler, testScheduler)
        )
    }

    @Test
    fun `when fetchCurrencyUseCase is build fetches from currencyRepo `() {
        on(
            currencyRepo.fetchExchangeRate(
                "EUR"
            )
        ).thenReturn(
            Single.just(
                GenericTestObjects.buildCurrencyExchangeRate()
            )
        )
        val testObserver = fetchCurrencyUseCase.get(Currency("EUR", BigDecimal("1"))).test()
        testScheduler.triggerActions()
        testObserver.assertValue {
            it.exchangeRate.size == 4 && it.selectedCurrency.id == "EUR"
        }
        verify(currencyRepo, times(1)).fetchExchangeRate("EUR")
        testObserver.dispose()
    }

}