package com.playground.currencies.domain.interactors

import com.playground.currencies.GenericTestObjects
import com.playground.currencies.TestDebugTransformer
import com.playground.currencies.TestSchedulerTransformer
import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.transformer.DebugTransformer
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import org.mockito.Mockito.`when` as on

@RunWith(MockitoJUnitRunner.Silent::class)
class CurrencyRefreshUseCaseTest {

    @Mock
    lateinit var fetchOnIntervalUseCase:FetchOnIntervalUseCase

    @InjectMocks
    lateinit var currencyRefreshUseCase:CurrencyRefreshUseCase

    private val debugTransformer: DebugTransformer = TestDebugTransformer()
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setUp(){
        testScheduler = TestScheduler()
        currencyRefreshUseCase.setDebugTransformer(debugTransformer)
        currencyRefreshUseCase.setSchedulerTransformer(
            TestSchedulerTransformer(testScheduler,testScheduler)
        )
    }

    @Test
    fun `when selected id changed switch to new fetch stream`(){
        val subject = PublishSubject.create<Currency>()
        val testCurrencyEur = Currency("EUR", BigDecimal("1"))
        val testCurrencyGdp = Currency("GDP", BigDecimal("1"))
        on(fetchOnIntervalUseCase.chain(testCurrencyEur))
            .thenReturn(
                Observable.interval(300,TimeUnit.SECONDS,testScheduler)
                    .map{ GenericTestObjects.buildCurrencyExchangeRate(selected = testCurrencyEur) }
            )
        on(fetchOnIntervalUseCase.chain(testCurrencyGdp))
            .thenReturn(
                Observable.interval(300,TimeUnit.SECONDS,testScheduler)
                    .map{ GenericTestObjects.buildCurrencyExchangeRate(selected = testCurrencyGdp) }
            )
        val testObserver = currencyRefreshUseCase.get(subject).test()

        testScheduler.advanceTimeBy(500,TimeUnit.SECONDS)

        subject.onNext(testCurrencyEur.copy())
        subject.onNext(testCurrencyEur.copy())

        testScheduler.advanceTimeBy(1000,TimeUnit.SECONDS)

        verify(fetchOnIntervalUseCase, times(1)).chain(testCurrencyEur)
        testObserver.assertValueCount(3)

        subject.onNext(testCurrencyGdp.copy())

        verify(fetchOnIntervalUseCase, times(1)).chain(testCurrencyGdp)

        testScheduler.advanceTimeBy(300,TimeUnit.SECONDS)

        testObserver.assertValueCount(4)
        testObserver.assertValueAt(3) { it.selectedCurrency.id == "GDP"}

        testScheduler.advanceTimeBy(300,TimeUnit.SECONDS)
        testObserver.assertValueCount(5)

        testObserver.dispose()
    }
}