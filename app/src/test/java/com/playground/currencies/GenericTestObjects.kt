package com.playground.currencies

import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.CurrencyExchangeRate
import java.math.BigDecimal

object GenericTestObjects {

    fun buildCurrencyExchangeRate(
        rate: String = "1.5",
        selected: Currency? = null
    ): CurrencyExchangeRate {
        return CurrencyExchangeRate(
            exchangeRate = generateDummyExchangeRate("GDP", "DKK", "RON", "USD"),
            selectedCurrency = selected ?: generateDummyCurrency(rate = rate)
        )
    }

    fun generateDummyExchangeRate(vararg currencyId: String): List<Currency> {
        return currencyId.map { Currency(it, BigDecimal("1.4")) }
    }

    fun generateDummyCurrency(id: String = "EUR", rate: String): Currency {
        return Currency(
            id = id,
            value = BigDecimal(rate)
        )
    }
}