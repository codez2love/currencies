package com.playground.currencies.domain.interactors

import com.playground.currencies.domain.transformer.DebugTransformer
import com.playground.currencies.domain.transformer.SchedulerTransformer
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

abstract class BaseUseCase {
    private lateinit var schedulerTransformer: SchedulerTransformer
    private lateinit var debugTransformer: DebugTransformer

    protected val isDebugLogsEnabled: Boolean
        get() = true

    @Inject
    fun setSchedulerTransformer(schedulerTransformer: SchedulerTransformer) {
        this.schedulerTransformer = schedulerTransformer
    }

    fun getBackgroundScheduler(): Scheduler {
        return schedulerTransformer.getSubscribeScheduler()
    }

    @Inject
    fun setDebugTransformer(debugTransformer: DebugTransformer) {
        this.debugTransformer = debugTransformer
    }

    protected fun schedulerTransformer(): SchedulerTransformer {
        return schedulerTransformer
    }

    protected fun debugTransformer(): DebugTransformer {
        return debugTransformer
    }

    fun Completable.applySubscribeScheduler(): Completable {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Observable<T>.applySubscribeScheduler(): Observable<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Single<T>.applySubscribeScheduler(): Single<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }
}


abstract class SingleParameterisedUseCase<T, P> : BaseUseCase() {

    /**
     * Builds an [Single] which will be used when executing the current [ObservableUseCase].
     */
    protected abstract fun build(params: P): Single<T>

    private fun make(params: P?, wrap: Boolean): Single<T> {
        if (params == null) {
            throw IllegalArgumentException("Params must be defined")
        }

        val single = wrapDebug(build(params))
        return if (wrap) wrap(single) else single
    }

    operator fun get(params: P): Single<T> {
        return make(params, true)
    }

    open fun chain(params: P): Single<T> {
        return make(params, false)
    }

    private fun wrap(observable: Single<T>): Single<T> {
        return observable.compose(schedulerTransformer().applySingleSchedulers())
    }

    private fun wrapDebug(observable: Single<T>): Single<T> {
        return if (isDebugLogsEnabled) {
            observable.compose(debugTransformer().applySingleDebugger(javaClass.simpleName))
        } else observable
    }
}

abstract class ObservableParameterisedUseCase<T, in P> : BaseUseCase() {

    /**
     * Builds an [Observable] which will be used when executing the current [ObservableUseCase].
     */
    protected abstract fun build(params: P): Observable<T>

    private fun make(params: P?, wrap: Boolean): Observable<T> {
        if (params == null) {
            throw IllegalArgumentException("Params must be defined")
        }

        val single = wrapDebug(build(params))
        return if (wrap) wrap(single) else single
    }

    operator fun get(params: P): Observable<T> {
        return make(params, true)
    }

    open fun chain(params: P): Observable<T> {
        return make(params, false)
    }

    private fun wrap(observable: Observable<T>): Observable<T> {
        return observable.compose(schedulerTransformer().applyObservableSchedulers<T>())
    }

    private fun wrapDebug(observable: Observable<T>): Observable<T> {
        return if (isDebugLogsEnabled) {
            observable.compose(debugTransformer().applyObservableDebugger(javaClass.simpleName))
        } else observable
    }
}

fun <T> Observable<T>.distinctUntilChangedOnHash(): Observable<T> {
    return this.distinctUntilChanged { first, sec -> first.hashCode() == sec.hashCode() }
}