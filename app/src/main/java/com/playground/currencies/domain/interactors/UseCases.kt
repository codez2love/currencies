package com.playground.currencies.domain.interactors

import com.playground.currencies.domain.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.Observables
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class FetchCurrencyUseCase @Inject constructor(
    private val currencyRepo: CurrencyRepo
) : SingleParameterisedUseCase<CurrencyExchangeRate, Currency>() {
    override fun build(params: Currency): Single<CurrencyExchangeRate> {
        return currencyRepo.fetchExchangeRate(params.id)
    }
}

class GetIntervalTriggerUseCase @Inject constructor() :
    ObservableParameterisedUseCase<Long, Int>() {
    override fun build(params: Int): Observable<Long> {
        return Observable
            .interval(params.toLong(), TimeUnit.SECONDS, getBackgroundScheduler())
            .startWith(1)
    }
}

class FetchOnIntervalUseCase @Inject constructor(
    private val fetchCurrencyUseCase: FetchCurrencyUseCase,
    private val getIntervalTriggerUseCase: GetIntervalTriggerUseCase,
    private val configProvider: ConfigProvider
) : ObservableParameterisedUseCase<CurrencyExchangeRate, Currency>() {
    override fun build(currency: Currency): Observable<CurrencyExchangeRate> {
        return getIntervalTriggerUseCase.chain(configProvider.getPullRate())
            .flatMapSingle { fetchCurrencyUseCase.chain(currency) }
            .distinctUntilChangedOnHash() // Emitting only when changes in rates occur
            .applySubscribeScheduler()

    }
}

class CurrencyRefreshUseCase @Inject constructor(
    private val fetchOnIntervalUseCase: FetchOnIntervalUseCase
) : ObservableParameterisedUseCase<CurrencyExchangeRate, Observable<Currency>>() {
    override fun build(inputStream: Observable<Currency>): Observable<CurrencyExchangeRate> {
        return inputStream
            .distinctUntilChanged { first, sec -> first.id == sec.id } //Only care about changes in id "EUR"->"GDP"
            .switchMap { fetchOnIntervalUseCase.chain(it) }
    }
}

class ConvertCurrencyUseCase @Inject constructor(
    private val currencyRefresh: CurrencyRefreshUseCase,
    private val currencyConverter: CurrencyConverter
) : ObservableParameterisedUseCase<CurrencyExchangeRate, Observable<Currency>>() {
    override fun build(inputStream: Observable<Currency>): Observable<CurrencyExchangeRate> {
        return Observables.combineLatest(
            source1 = inputStream.distinctUntilChanged { first, sec -> first.id != sec.id },
            // Ignoring when a currency Id changed as this will trigger an event in source2 so it will piggy back of the response from there
            source2 = currencyRefresh.chain(inputStream),
            combineFunction = currencyConverter::applyRates
        )
    }
}


