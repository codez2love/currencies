package com.playground.currencies.domain.transformer

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import timber.log.Timber
import javax.inject.Inject

interface DebugTransformer {
    fun <T> applyObservableDebugger(tag: String): ObservableTransformer<T, T>
    fun applyCompletableDebugger(tag: String): CompletableTransformer
    fun <T> applySingleDebugger(tag: String): SingleTransformer<T, T>
}


class DebugTimberTransformer @Inject constructor() : DebugTransformer {

    override fun <T> applyObservableDebugger(tag: String): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it
                .doOnNext { data ->
                    Timber.v(
                        "[%s] [onNext] [Thread: %s] [observable] [response: %s]",
                        tag,
                        Thread.currentThread().name,
                        data.toString()
                    )
                }
                .doOnSubscribe {
                    Timber.v(
                        "[%s] [subscribe] [Thread:%s] [observable]",
                        tag,
                        Thread.currentThread().name
                    )
                }
                .doOnDispose {
                    Timber.v("[%s] [dispose] [observable]", tag)
                }
        }
    }

    override fun applyCompletableDebugger(tag: String): CompletableTransformer {
        return CompletableTransformer {
            it
                .doOnComplete {
                    Timber.v(
                        "[%s] [onComplete] [Thread:%s] [completable]",
                        tag,
                        Thread.currentThread().name
                    )
                }
                .doOnSubscribe {
                    Timber.v(
                        "[%s] [subscribe] [Thread:%s] [completable]",
                        tag,
                        Thread.currentThread().name
                    )
                }
                .doOnDispose {
                    Timber.v("[%s] [dispose] [completable]", tag)
                }
        }
    }

    override fun <T> applySingleDebugger(tag: String): SingleTransformer<T, T> {
        return SingleTransformer {
            it
                .doOnSuccess { data ->
                    Timber.d(
                        "[%s] [onSuccess] [Thread:%s] [single] [response: %s] ",
                        tag,
                        Thread.currentThread().name,
                        data.toString()
                    )
                }
                .doOnSubscribe {
                    Timber.v(
                        "[%s] [subscribe] [Thread:%s] [single]",
                        tag,
                        Thread.currentThread().name
                    )
                }
                .doOnDispose {
                    Timber.v("[%s] [dispose] [single]", tag)
                }
        }
    }
}
