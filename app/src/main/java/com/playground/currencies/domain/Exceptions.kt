package com.playground.currencies.domain

class NoInternetException(
    cause: Throwable? = null
) : RuntimeException("No internet found :(", cause)

class ExceptionNotFound(
    cause: Throwable? = null
) : RuntimeException("Wtf, Exception not ofound?", cause)
