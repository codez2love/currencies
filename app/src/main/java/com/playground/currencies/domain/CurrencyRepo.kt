package com.playground.currencies.domain

import io.reactivex.Single
import java.math.BigDecimal

interface CurrencyRepo {
    fun fetchExchangeRate(currencyId: String): Single<CurrencyExchangeRate>
}

interface CurrenciesClient {
    fun fetchCurrencyList(currencyId: String): Single<List<Currency>>
}

interface CurrencyConverter {
    fun applyRates(selectedCurrency: Currency, rates: CurrencyExchangeRate): CurrencyExchangeRate
}

interface ConfigProvider {
    fun getPullRate():Int
}

data class Currency(
    val id: String,
    val value: BigDecimal,
    val rate: BigDecimal = value
)

data class CurrencyExchangeRate(
    val exchangeRate: List<Currency>,
    val selectedCurrency: Currency
) {
    override fun toString(): String {
        val lastItemData = if (exchangeRate.isNotEmpty()) {
            with(exchangeRate[exchangeRate.size - 1]) {
                "${this.id} = ${this.value}"
            }
        } else {
            "Empty"
        }
        return "HashCode= ${hashCode()} for Selected=${selectedCurrency.id}  ====== $lastItemData"
    }
}