package com.playground.currencies.domain.transformer

import com.playground.currencies.domain.PostExecutionThread
import com.playground.currencies.domain.ThreadExecutor
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

interface SchedulerTransformer {
    fun <T> applyObservableSchedulers(): ObservableTransformer<T, T>
    fun applyCompletableSchedulers(): CompletableTransformer
    fun <T> applySingleSchedulers(): SingleTransformer<T, T>
    fun applySubscribeScheduler(completable: Completable): Completable
    fun <T> applySubscribeScheduler(single: Single<T>): Single<T>
    fun <T> applySubscribeScheduler(observable: Observable<T>): Observable<T>
    fun getSubscribeScheduler():Scheduler
}

class GenericSchedulerTransformer @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SchedulerTransformer {

    private val subscribeScheduler = Schedulers.from(threadExecutor)
    private val observeScheduler = postExecutionThread.scheduler

    override fun <T> applyObservableSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer {
            it
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }
    }

    override fun applyCompletableSchedulers(): CompletableTransformer {
        return CompletableTransformer {
            it
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }
    }

    override fun <T> applySingleSchedulers(): SingleTransformer<T, T> {
        return SingleTransformer {
            it
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }
    }

    override fun applySubscribeScheduler(completable: Completable): Completable {
        return completable.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(single: Single<T>): Single<T> {
        return single.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(observable: Observable<T>): Observable<T> {
        return observable.subscribeOn(subscribeScheduler)
    }

    override fun getSubscribeScheduler(): Scheduler {
        return subscribeScheduler
    }
}