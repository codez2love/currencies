package com.playground.currencies.presentation.feature.start

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.playground.currencies.R
import com.playground.currencies.presentation.feature.calculator.CalculatorFragment
import dagger.android.DaggerActivity
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CalculatorFragment.newInstance())
                .commitNow()
        }

    }

}
