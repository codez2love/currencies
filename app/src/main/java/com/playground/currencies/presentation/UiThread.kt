package com.playground.currencies.presentation

import com.playground.currencies.domain.PostExecutionThread
import com.playground.currencies.presentation.dagger.AppScope
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@AppScope
class UiThread @Inject
constructor() : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}