package com.playground.currencies.presentation.comon

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener

class DecimalEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = android.R.attr.editTextStyle
) : EditText(context, attrs, defStyleAttr) {
    private var isAdded = false

    private var onTextChanged: ((String) -> Unit)? = null

    private val disabledEnteryKey =
        OnEditorActionListener { _, actionId, _ -> actionId == EditorInfo.IME_ACTION_DONE }
    private val textWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (isAdded && onTextChanged != null)
                onTextChanged!!(s.toString())
            Utils.insertCommaIntoNumber(this@DecimalEditText, s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    init {
        setOnEditorActionListener(disabledEnteryKey)
    }

    fun setOnTextChanged(onTextChanged: (String) -> Unit) {
        this.onTextChanged = onTextChanged
    }

    fun setEditing(isEnabled: Boolean) {
        if (isEnabled) {
            if (!isAdded) {
                addTextChangedListener(textWatcher)
                isAdded = true
            }
            if (requestFocus()) {
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                )
            }
        } else {
            if (isAdded) {
                removeTextChangedListener(textWatcher)
                isAdded = false
            }
        }
    }

}