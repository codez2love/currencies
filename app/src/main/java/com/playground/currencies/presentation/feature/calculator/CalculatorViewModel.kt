package com.playground.currencies.presentation.feature.calculator

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.playground.currencies.presentation.feature.Resource
import javax.inject.Inject

class CalculatorViewModel @Inject constructor(
    private val calculatorLiveData: CalculatorLiveData
) : ViewModel() {

    fun getCalculatorViewData(): LiveData<Resource<CurrencyViewData>> {
        return calculatorLiveData
    }
}
