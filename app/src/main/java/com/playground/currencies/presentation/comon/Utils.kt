package com.playground.currencies.presentation.comon

import android.widget.EditText
import java.text.DecimalFormat

object Utils {

    fun insertCommaIntoNumber(etText: EditText, s: CharSequence) {
        try {
            if (s.toString().isNotEmpty()) {
                var convertedStr = s.toString()
                if (s.toString().contains(".")) {
                    if (chkConvert(s.toString()))
                        convertedStr =
                            customFormat(
                                "###,###.##",
                                java.lang.Double.parseDouble(s.toString().replace(",", ""))
                            )
                } else {
                    convertedStr = customFormat(
                        "###,###.##",
                        java.lang.Double.parseDouble(s.toString().replace(",", ""))
                    )
                }

                if (etText.text.toString() != convertedStr && convertedStr.isNotEmpty()) {
                    etText.setText(convertedStr)
                }
                etText.setSelection(etText.text.length)
            }

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    private fun customFormat(pattern: String, value: Double): String {
        val myFormatter = DecimalFormat(pattern)
        return myFormatter.format(value)
    }

    private fun chkConvert(s: String): Boolean {
        val tempArray = s.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (tempArray.size > 1) {
            Integer.parseInt(tempArray[1]) > 0
        } else
            false
    }
}