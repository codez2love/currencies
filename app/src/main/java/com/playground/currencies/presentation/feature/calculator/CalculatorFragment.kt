package com.playground.currencies.presentation.feature.calculator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.playground.currencies.R
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.main_fragment.*
import timber.log.Timber
import javax.inject.Inject


class CalculatorFragment : DaggerFragment() {

    companion object {
        const val calculatorState = "calcState"
        fun newInstance() = CalculatorFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var currencyAdapter: CurrencyAdapter

    private lateinit var viewModel: CalculatorViewModel
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView.adapter = currencyAdapter
        compositeDisposable += currencyAdapter.observeCurrencyState().subscribeBy(
            onNext = { scrollToTop() },
            onError = { Timber.e(it) }
        )
    }

    private fun scrollToTop() {
        recyclerView.layoutManager?.scrollToPosition(0)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(CalculatorViewModel::class.java)
        viewModel.getCalculatorViewData().observe(this, Observer { currencyAdapter.setListData(it) })

        if (savedInstanceState == null) {
            CalculatorInputStream.stream.onNext(CurrencyItem())
        } else {
            restoreState(savedInstanceState)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(calculatorState, CalculatorInputStream.curInputState)
    }

    private fun restoreState(savedInstanceState: Bundle) {
        savedInstanceState.getParcelable<CalculatorViewState>(calculatorState)?.let {
            CalculatorInputStream.stream.onNext(CurrencyViewDataTransformer.fromViewState(it))
        }
    }
}


