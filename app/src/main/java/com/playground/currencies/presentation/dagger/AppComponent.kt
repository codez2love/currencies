package com.playground.currencies.presentation.dagger

import android.content.Context
import androidx.fragment.app.Fragment
import com.playground.currencies.data.DataModule
import com.playground.currencies.domain.JobExecutor
import com.playground.currencies.domain.PostExecutionThread
import com.playground.currencies.domain.ThreadExecutor
import com.playground.currencies.domain.transformer.DebugTimberTransformer
import com.playground.currencies.domain.transformer.DebugTransformer
import com.playground.currencies.domain.transformer.GenericSchedulerTransformer
import com.playground.currencies.domain.transformer.SchedulerTransformer
import com.playground.currencies.presentation.CurrenciesApp
import com.playground.currencies.presentation.UiThread
import com.playground.currencies.presentation.feature.calculator.CalculatorFragment
import com.playground.currencies.presentation.feature.start.MainActivity
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Scope

@AppScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        Bindings::class,
        ApplicationModule::class
    ]
)
interface AppComponent : AndroidInjector<CurrenciesApp> {
    override fun inject(instance: CurrenciesApp)
}

@Module(
    includes = [
        DataModule::class
    ]
)
class ApplicationModule(
    private val context: Context
) {
    @Provides
    fun provideAppContext(): Context {
        return context
    }

    @Provides
    fun provideUiThread(uiThread:UiThread):PostExecutionThread = uiThread

    @Provides
    fun provideExecutor(jobExecutor: JobExecutor):ThreadExecutor = jobExecutor

    @Provides
    fun provideSchedulerTransformer(defaultTransformer: GenericSchedulerTransformer):SchedulerTransformer = defaultTransformer

    @Provides
    fun provideDebugTransformer(debugTimber: DebugTimberTransformer):DebugTransformer = debugTimber

    @Provides
    fun provideBackgroundScheduler(threadExecutor: ThreadExecutor):Scheduler{
        return Schedulers.from(threadExecutor)
    }
}

@Module(
    includes = [
        ViewModelModule::class
    ]
)
abstract class Bindings {

    @StartActivityScope
    @ContributesAndroidInjector(modules = [StartActivityModule::class])
    abstract fun startActivity(): MainActivity


}

@Module
abstract class StartActivityModule {

    @CalculatorScope    
    @ContributesAndroidInjector(modules = [CalculatorModule::class])
    abstract fun calculatorFragment(): CalculatorFragment
}

@Module
abstract class CalculatorModule {

    @Binds
    abstract fun bindFragment(fragment: CalculatorFragment): Fragment
}

@Scope
annotation class AppScope

@Scope
annotation class StartActivityScope

@Scope
annotation class CalculatorScope