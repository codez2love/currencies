package com.playground.currencies.presentation.feature.calculator

import android.os.Parcelable
import com.playground.currencies.domain.Currency
import com.playground.currencies.domain.CurrencyExchangeRate
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

@Parcelize
data class CalculatorViewState(
    val selectedCurrency: String,
    val value: String
) : Parcelable

data class CurrencyViewData(
    val currencies: List<CurrencyItem>,
    val selectedItem: CurrencyItem
)

data class CurrencyItem(
    val name: String = "EUR",
    val value: String = "1.0",
    val fullName: String = name,
    val rate: String = "1 $name = $value",
    var isSelected: Boolean = false
)

object CurrencyViewDataTransformer {
    private val decimalFormatter = DecimalFormat("###,###.##")

    fun toDomain(item: CurrencyItem): Currency {
        return Currency(item.name, BigDecimal(item.value.replace(",", "")))
    }

    fun fromDomain(exchangeRate: CurrencyExchangeRate): CurrencyViewData {
        return CurrencyViewData(
            currencies = exchangeRate.exchangeRate.map { fromDomain(it) },
            selectedItem = fromDomain(exchangeRate.selectedCurrency).copy(isSelected = true)
        )
    }

    fun fromDomain(currency: Currency): CurrencyItem {
        return CurrencyItem(
            name = currency.id,
            fullName = buildCurrencyRate(currency.id),
            value = toDisplayFormat(currency.value),
            rate = "1 ${currency.id} = ${toDisplayFormat(currency.rate)}"
        )
    }

    fun toDisplayFormat(currency: BigDecimal): String {
        return decimalFormatter.format(
            currency.setScale(2, RoundingMode.HALF_UP)
        )
    }

    fun toViewState(currencyItem: CurrencyItem): CalculatorViewState {
        return CalculatorViewState(currencyItem.name, currencyItem.value)
    }

    fun fromViewState(viewState: CalculatorViewState): CurrencyItem {
        return CurrencyItem(
            name = viewState.selectedCurrency,
            value = viewState.value
        )
    }

    private fun buildCurrencyRate(id: String): String {
        try {
            val curr = java.util.Currency.getInstance(id)
            return curr.displayName
        } catch (e: IllegalArgumentException) {
        }
        return id
    }
}


