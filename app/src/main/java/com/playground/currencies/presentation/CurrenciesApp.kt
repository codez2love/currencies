package com.playground.currencies.presentation

import com.playground.currencies.presentation.dagger.ApplicationModule
import com.playground.currencies.presentation.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class CurrenciesApp : DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}
//TODO List
// Improve inputValue EditText keyboard handling
// Try out Data-binding
// Improve error handling (add for network, and error screen)
// pull down for refresh, progress and error
// More Testing

