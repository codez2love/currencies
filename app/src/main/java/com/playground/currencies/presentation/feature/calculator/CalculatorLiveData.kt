package com.playground.currencies.presentation.feature.calculator

import androidx.lifecycle.LiveData
import com.playground.currencies.domain.interactors.ConvertCurrencyUseCase
import com.playground.currencies.presentation.feature.Resource
import com.playground.currencies.presentation.feature.ResourceState
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class CalculatorLiveData @Inject constructor(
    private val convertCurrencyUseCase: ConvertCurrencyUseCase
) : LiveData<Resource<CurrencyViewData>>() {

    private var disposable: Disposable? = null

    override fun onActive() {
        disposable?.let { if (!it.isDisposed) it.dispose() }
        disposable = buildCalculatorResultObs().subscribeBy(
            onNext = {
                value = Resource(
                    state = ResourceState.SUCCESS,
                    data = it
                )
            },
            onError = {
                value = Resource(
                    state = ResourceState.ERROR,
                    message = it.message
                )
                Timber.e(it, "Big bobo")
            }
        )
    }

    private fun buildCalculatorResultObs(): Observable<CurrencyViewData> {
        return convertCurrencyUseCase[
                CalculatorInputStream.stream.map {
                    CurrencyViewDataTransformer.toDomain(it)
                }
        ]
            .map { CurrencyViewDataTransformer.fromDomain(it) }
    }

    override fun onInactive() {
        disposable?.dispose()
    }
}