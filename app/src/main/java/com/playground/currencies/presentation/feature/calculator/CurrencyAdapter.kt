package com.playground.currencies.presentation.feature.calculator

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.playground.currencies.R
import com.playground.currencies.presentation.comon.DecimalEditText
import com.playground.currencies.presentation.comon.Utils
import com.playground.currencies.presentation.feature.Resource
import com.playground.currencies.presentation.feature.ResourceState
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject


class CurrencyAdapter @Inject constructor(
) : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    private val viewData: MutableList<CurrencyItem> = mutableListOf()

    private val listClickObservable = BehaviorSubject.create<String>()
    private val valueObservable = BehaviorSubject.create<String>()

    init {
        observeCurrencyState()
            .subscribe(CalculatorInputStream.stream)
    }

    fun observeCurrencyState(): Observable<CurrencyItem> {
        return Observables.combineLatest(
            observeItemClick()
                .startWith(CalculatorInputStream.curInputState.selectedCurrency),
            valueObservable
                .startWith(CalculatorInputStream.curInputState.value)
                .filter { it.isNotEmpty() }
                .filter { !it.endsWith(".") }
        ) { currency, value -> CurrencyItem(currency, value) }
            .doOnNext {
                CalculatorInputStream.curInputState = CurrencyViewDataTransformer.toViewState(it)
            }
            .distinctUntilChanged { first, sec -> first.hashCode() == sec.hashCode() }
    }

    private fun observeItemClick(): Observable<String> {
        return listClickObservable
            .doOnNext { valueObservable.onNext("1") }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.iv_calc_currency, parent, false)
        return CurrencyViewHolder(
            itemView = itemView,
            onItemClick = { listClickObservable.onNext(it) },
            onTextChanged = { valueObservable.onNext(it) }
        )
    }

    override fun getItemCount(): Int {
        return viewData.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(viewData[position])
    }

    fun setListData(data: Resource<CurrencyViewData>) {
        when (data.state) {
            ResourceState.SUCCESS -> {
                with(data.data!!) {
                    handleSuccess(this)
                }
            }
            ResourceState.LOADING -> {
            }
            ResourceState.ERROR -> {
                Timber.e("Error....")
            }
        }
    }

    private fun handleSuccess(data: CurrencyViewData) {
        if (viewData.isEmpty()) {
            viewData.add(data.selectedItem)
            viewData.addAll(data.currencies)
            notifyDataSetChanged()
        } else {
            if (viewData[0].name != data.selectedItem.name) {
                setSelectedCurrency(data.selectedItem.name, data.selectedItem.value)
            }
            viewData.removeAll(viewData.drop(1))
            viewData.addAll(data.currencies)
            notifyItemRangeChanged(1, data.currencies.size)
        }
    }

    private fun setSelectedCurrency(currencyId: String, value: String? = null) {
        val positionOfSelectedItem = viewData.indexOfFirst { it.name == currencyId }
        val newSelectedItem = viewData[positionOfSelectedItem]
        viewData.add(
            0,
            newSelectedItem.copy(
                isSelected = true,
                value = value ?: viewData[0].value
            )
        )
        notifyItemMoved(positionOfSelectedItem, 0)
        notifyItemChanged(0)

        if (viewData[1].isSelected) {
            viewData.removeAt(1)
            notifyItemRemoved(1)
        }
    }

    class CurrencyViewHolder(
        itemView: View,
        onItemClick: (String) -> Unit,
        val onTextChanged: (String) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        private val currencyTitle: TextView = itemView.findViewById(R.id.currency_title)
        private val currencySubtitle: TextView = itemView.findViewById(R.id.currency_subtitle)
        private val exchangeResult: TextView = itemView.findViewById(R.id.exchange_result)
        private val currencyRate: TextView = itemView.findViewById(R.id.currency_rate)
        private val inputValue: DecimalEditText = itemView.findViewById(R.id.value_input)

        init {
            itemView.setOnClickListener { onItemClick(it.tag as String) }
            inputValue.setOnTextChanged(onTextChanged)
        }

        fun bind(currencyItem: CurrencyItem) {
            currencyTitle.text = currencyItem.name
            currencySubtitle.text = currencyItem.fullName
            exchangeResult.text = currencyItem.value
            currencyRate.text = currencyItem.rate
            with(currencyItem.isSelected) {
                itemView.isSelected = this
                showInputView(this)
                inputValue.setEditing(this)
            }
            itemView.tag = currencyItem.name
        }

        private fun showInputView(isVisible: Boolean) {
            exchangeResult.isVisible = !isVisible
            currencyRate.isVisible = !isVisible
            inputValue.isVisible = isVisible
        }
    }
}

//Move this from AppScope/Singleton to ViewModel scope
object CalculatorInputStream {
    var curInputState: CalculatorViewState = CalculatorViewState("EUR", "1")
    val stream = BehaviorSubject.create<CurrencyItem>()
}
