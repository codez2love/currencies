package com.playground.currencies.data

import com.playground.currencies.data.net.CurrenciesRatesClient
import com.playground.currencies.data.net.ExchangeRatesAPI
import com.playground.currencies.data.store.ConfigProviderImpl
import com.playground.currencies.data.store.CurrencyConverterImpl
import com.playground.currencies.data.store.CurrencyRepository
import com.playground.currencies.domain.ConfigProvider
import com.playground.currencies.domain.CurrenciesClient
import com.playground.currencies.domain.CurrencyConverter
import com.playground.currencies.domain.CurrencyRepo
import com.playground.currencies.presentation.dagger.AppScope
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

@Module(
    includes = [
        NetModule::class
    ]
)
abstract class DataModule {
    @Binds
    abstract fun bindRepository(currencyRepository: CurrencyRepository): CurrencyRepo

    @Binds
    abstract fun bindConverter(currencyConverter: CurrencyConverterImpl): CurrencyConverter

    @Binds
    abstract fun bindConfigProvider(configProvider: ConfigProviderImpl): ConfigProvider
}


@Module
abstract class NetModule {

    @Binds
    abstract fun bindClient(currencyClient: CurrenciesRatesClient): CurrenciesClient

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun httpLoggingInterceptor(): HttpLoggingInterceptor {
            val logging = HttpLoggingInterceptor { message -> Timber.d(message) }
            return logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
        }

        @JvmStatic
        @Provides
        fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
            return OkHttpClient
                .Builder()
                .addInterceptor(loggingInterceptor)
                .retryOnConnectionFailure(true)
                .build()
        }

        @JvmStatic
        @Provides
        fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.exchangeratesapi.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }

        @JvmStatic
        @Provides
        @AppScope
        fun provideApi(retrofit: Retrofit): ExchangeRatesAPI {
            return retrofit.create(ExchangeRatesAPI::class.java)
        }
    }
}