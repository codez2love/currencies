package com.playground.currencies.data.store

import com.playground.currencies.domain.*
import io.reactivex.Single
import java.math.BigDecimal
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val currencyClient: CurrenciesClient
) : CurrencyRepo {

    override fun fetchExchangeRate(currencyId: String): Single<CurrencyExchangeRate> {
        return currencyClient.fetchCurrencyList(currencyId)
            .map {
                CurrencyExchangeRate(
                    exchangeRate = it,
                    selectedCurrency = Currency(currencyId, BigDecimal("1"))
                )
            }
    }
}

class CurrencyConverterImpl @Inject constructor() : CurrencyConverter {
    override fun applyRates(
        selectedCurrency: Currency,
        rates: CurrencyExchangeRate
    ): CurrencyExchangeRate {
        return CurrencyExchangeRate(
            exchangeRate = rates.exchangeRate
                .map {
                    Currency(
                        id = it.id,
                        value = it.value.multiply(selectedCurrency.value),
                        rate = it.value
                    )
                }
                .sortedBy { it.id }
                .filter { it.id != rates.selectedCurrency.id },
            selectedCurrency = rates.selectedCurrency.copy(value = selectedCurrency.value)
        )
    }
}

class ConfigProviderImpl @Inject constructor():ConfigProvider{
    override fun getPullRate(): Int {
        return 3
    }
}