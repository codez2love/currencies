package com.playground.currencies.data.net

import com.playground.currencies.domain.CurrenciesClient
import com.playground.currencies.domain.Currency
import com.playground.currencies.presentation.dagger.AppScope
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal
import javax.inject.Inject


interface ExchangeRatesAPI {

    @GET("latest")
    fun getCurrencies(@Query("base") base: String): Single<ExchangeRateResponseDTO>
}

data class ExchangeRateResponseDTO(
    val rates: Map<String, BigDecimal>,
    val base: String
)

class CurrenciesRatesClient @Inject constructor(
    private val restExchangeApi: ExchangeRatesAPI
) : CurrenciesClient {

    override fun fetchCurrencyList(currencyId: String): Single<List<Currency>> {
        return restExchangeApi.getCurrencies(currencyId)
            .map { dto -> dto.rates.map { Currency(it.key, it.value) } }
    }
}